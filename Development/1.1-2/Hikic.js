/* Powered By 2018-Skyogo Studio */
/* Hikic v1.1 Development */
/* Create Time: 2018-3-9 */
/* Skyogo Website: http://www.skyogo.com */
/* Welcome to use Hikic! */ 
var lazyLoadRange = 20;
var lazyLoadLRT = 50;
function _(element) {
    this.element = element;
    this.val = val;
    this.attr = attr;
    this.text = text;
    this.html = html;
    this.innerHeight = innerHeight;
    this.innerWidth = innerWidth;
    this.width = width;
    this.css = css;
    this.addClass = addClass;
    this.click = click;
    this.children = children;
    this.parent = parent;
    this.hasClass = hasClass;
    this.removeAttr = removeAttr;
    this.removeClass = removeClass;
    this.removeHTML = removeHTML;
    this.empty = empty;
    this.height = height;
    this.load = load;
    this.cookie = cookie;
    this.getCookieVal = getCookieVal;
    this.delCookie = delCookie;
    this.mousemove = mousemove;
    this.mousedown = mousedown;
    this.mouseout = mouseout;
    this.mouseover = mouseover;
    this.ready = ready;
    this.mouseup = mouseup;
    this.focus = focus;
    this.blur = blur;
    this.hide = hide;
    this.show = show;
    this.lazyLoad = lazyLoad;
    this.setLazyLoadLRT = setLazyLoadLRT;//LRT = ListenerRepeatTime-设置懒加载监听器重复时间间隔
    this.setLazyLoadRange = setLazyLoadRange;
    function val(value){//获取value 设置value
        if(value===null||value===""||value===undefined){
            return document.querySelector(this.element).value;
        }else{
            document.querySelector(this.element).value = value;
        }
    }
    function html(value){//获取html 设置html
        if(value===null||value===""||value===undefined){
            return document.querySelector(this.element).innerHTML;
        }else{
            document.querySelector(this.element).innerHTML = value;
        }
    }
    function text(value){//获取text 设置text
        if(value===null||value===""||value===undefined){
           return document.querySelector(this.element).innerText;
        }else{
            document.querySelector(this.element).innerText = value;
        }
    }
    function attr(attrName,value){//获取属性 设置属性
        if(attrName===null||attrName===""||attrName===undefined){
           return "parameter error";
        }else if(value===null||value===""||value===undefined){
            return document.querySelector(this.element).getAttribute(attrName);
        }else{
            document.querySelector(this.element).setAttribute(attrName,value);
        }
    }
    function addClass(value){//添加class属性值，如果想添加多个，请使用分号分隔
        var addClassArr = value.split(";");
        for(var i=0;i<addClassArr.length;i++){
            document.querySelector(this.element).className = document.querySelector(this.element).className+" "+addClassArr[i];
        }
    }
    function hasClass(className){//检查是否含有这个class
        var classArr = document.querySelector(this.element).className.toString().split(" ");
        for(var i=0;i<classArr.length;i++){
            if(classArr[i] === className){
                return true;
            }
        }
        return false;
    }
    function removeAttr(attrName){//删除dom属性
        document.querySelector(this.element).removeAttribute(attrName);
    }
    function removeClass(className){//删除class属性
        var classVal = document.querySelector(this.element).className.replace(className,"");
        document.querySelector(this.element).setAttribute("class",classVal);
    }
    function removeHTML(){//移除dom
        document.querySelector(this.element).remove();
    }
    function empty(){//删除dom的全部子节点
        document.querySelector(this.element).innerHTML = "";
    }
    function css(cssName,value){//获取或设置所选元素的css属性
        if(cssName===null||cssName===undefined||cssName===""){
            return "parameter error";
        }else if(value===""||value===undefined||value===null){
            var cssValue;
            eval("cssValue = document.querySelector('"+this.element+"').style."+cssName);
            return cssValue;
        }else{
            eval("document.querySelector(this.element).style."+cssName+"='"+value+"';");
        }
    }
    function height(value){//获取或设置所选元素的height属性
        if(value===null||value===undefined||value===""){
            return document.querySelector(this.element).style.height;
        }else{
            document.querySelector(this.element).style.height = value;
        }
    }
    function width(value){//获取或设置所选元素的width属性
        if(value===null||value===undefined||value===""){
            return document.querySelector(this.element).style.width;
        }else{
            document.querySelector(this.element).style.width = value;
        }
    }
    function innerHeight(){//获取元素内部高度
            return document.querySelector(this.element).clientHeight;
    }
    function innerWidth(){//获取元素内部宽度
        return document.querySelector(this.element).clientWidth;
    }
    function parent(){//获取dom的父元素
        return document.querySelector(this.element).parentNode.nodeName;
    }
    function children(){//获取dom的全部子元素
        var childNodesArr = document.querySelector(this.element).childNodes;
        var childNodesRealArr = new Array();
        for(var i=0;i<childNodesArr.length;i++){
            childNodesRealArr[childNodesRealArr.length] = childNodesArr[i].nodeName;
        }
        return childNodesRealArr;
    }
    function load(URL,RequestMethod,Asynchronous){//ajax方法，并加载到dom里
        var xmlhttp;
        if (window.XMLHttpRequest){
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }else{
            // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        if(Asynchronous){
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.querySelector(element).innerHTML=xmlhttp.responseText;
                }
            }
        }
        xmlhttp.open(RequestMethod,URL,Asynchronous);
        xmlhttp.send();
        if(!Asynchronous){
            document.querySelector(element).innerHTML=xmlhttp.responseText;
        }
    }
    function cookie(cookieName,cookieVal,ExpiryTime,path){//设置和替换cookie，（path不填写则默认为/）,如果cookie名称一样则替换
        if(path===null||path===undefined||path===""){
           path = "/";
        }
        document.cookie = cookieName+"="+cookieVal+"; expires="+ExpiryTime+"; path="+path+";";
    }
    function getCookieVal(cookieName){//获取cookie的值
        var arrstr = document.cookie.split("; ");
        for (var i = 0; i < arrstr.length; i++) {
            var temp = arrstr[i].split("=");
            if (temp[0] == cookieName) {
                return temp[1]
            }
        }
    }
    function delCookie(cookieName){//删除cookie
        if(getCookieVal(cookieName)===null||getCookieVal(cookieName)===undefined||getCookieVal(cookieName)===""){
            return;
        }
        cookie(cookieName,"","Thu, 18 Dec 2000 12:00:00 GMT");
    }
    function click(code){//设置点击事件
        eval("document.querySelector('"+this.element+"').onclick = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function mousemove(code){//设置鼠标移动事件
        eval("document.querySelector('"+this.element+"').onmousemove = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function mousedown(code){//设置鼠标按下事件
        eval("document.querySelector('"+this.element+"').onmousedown = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function mouseup(code){//设置鼠标抬起事件
        eval("document.querySelector('"+this.element+"').onmouseup = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function mouseout(code){//设置鼠标移出事件
        eval("document.querySelector('"+this.element+"').onmouseout = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function mouseover(code){//设置鼠标移入事件
        eval("document.querySelector('"+this.element+"').onmouseover = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function ready(code){//设置加载完成事件
        eval("document.querySelector('"+this.element+"').onload = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function focus(code){//设置获得焦点事件
        eval("document.querySelector('"+this.element+"').onfocus = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function blur(code){//设置失去焦点事件
        eval("document.querySelector('"+this.element+"').onblur = "+code.toString().replace(/[\r\n]/g, ""));
    }
    function hide(){//隐藏元素
        document.querySelector(this.element).style.display = "none";
    }
    function show(displayName){//显示元素
        if(displayName === "inline"){
            document.querySelector(this.element).style.display = "inline-block";
        }else{
            document.querySelector(this.element).style.display = "block";
        }
    }
    var lazyLoadArr = new Array();
    function lazyLoad(url){//设置懒加载，保持性能
        lazyLoadArr[lazyLoadArr.length] = this.element;
        lazyLoadArr[lazyLoadArr.length] = url;
    }
    function setLazyLoadLRT(interval){//设置懒加载监听器时间间隔
        lazyLoadLRT = interval;
    }
    function setLazyLoadRange(PX){//设置懒加载的资源距离进入屏幕范围还剩多少px时就加载，默认为20
        lazyLoadRange = PX;
    }
    setInterval(function(){//懒加载监听器
        if(lazyLoadArr.length!==0){
        for(var i=0;i<lazyLoadArr.length;i++){
            if(i%2 === 0){//如果i取余2等于0，那么就执行以下操作
                var Element = document.querySelector(lazyLoadArr[i]);
                var h = window.screen.availHeight;
                if(Element.getBoundingClientRect().top-(h+lazyLoadRange) <= 0){
                    Element.setAttribute("src",lazyLoadArr[i+1]);
                    lazyLoadArr.splice(0,2);
                    i-=2;
                }
            }
        }
    }
    },lazyLoadLRT)
    if (this===window) {
        return new _(element);
    }
}
//获取Hikic信息方法，记得修改内容
function Hikic(){
    this.createTime = createTime;
    this.version = version;
    function version(){//获取版本
        return "v1.1 Development"
    }
    function createTime(){//获取创建时间
        return "2018-3-9";
    }
    if (this===window) {
        return new Hikic();
    }
}
/* the end */
