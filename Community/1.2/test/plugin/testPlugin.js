/*Hikic插件编写步骤：

1、先使用HikicPlugin对象里的join方法，使你的插件加入到Hikic插件组里面（必须设置）
2、使用HikicPlugin对象里的setParameter方法，配置一些插件的参数，第一个参数为pluginName（要和在插件组里注册的名字一样），第二个参数为version，第三个参数为createTime（必须设置）
3、直接使用_.prototype.方法名称 = function(){}给_类添加一个Hikic方法即可
-----就这么简单------

其他函数或参数：


其他事项：
如何获取Hikic.js的元素选择器的值？
使用this.element即可获取
如何使用Hikic.js里的方法？
直接按原来的方式来写即可，例如_("#test").attr("value","hello");

为什么要使用Hikic编写插件？
因为你可以直接让你的产品被Hikic的用户了解到，并且可以很方便的获取元素选择器，和调用Hikic内置方法去完成效果
*/

//第一步
HikicPlugin().join("testPlugin");//加入Hikic插件组（必须设置）

//第二步
HikicPlugin().setParameter("testPlugin",1.0,"2018-3-14");//设置插件组插件参数，设置参数前必须先加入Hikic插件组！！！（必须设置）

//第三步
_.prototype.test = function(){//添加test方法
    console.log(this.element);//用this.element获取用户传入的元素选择器的值
    console.log(HikicPlugin().getJoin("testPlugin"));//用HikicPlugin对象里的getJoin方法获取单个插件或所有插件的名称，如果getJoin参数不填写则为获取所有插件
    console.log(HikicPlugin().getParameter("version","testPlugin"));//使用这个方法获取插件参数，如果pluginName参数不填写则为获取所有插件，第一个参数为插件名称（要和在插件组里注册的名字一样），第二个参数为“参数名称”（version、createTime），第三个参数为“插件名称”
}

/*Hikic鼓励大家多多参与Hikic的插件制作，感谢大家！*/
